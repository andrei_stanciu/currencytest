package com.`as`.currency.network

import com.`as`.currency.currencies.model.CurrenciesResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface RetrofitCurrencyApi {

    @GET("android/latest?base=EUR")
    fun getCurrencies(): Observable<CurrenciesResponse>

}