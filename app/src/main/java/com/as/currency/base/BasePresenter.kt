package com.`as`.currency.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter <V:  BasePresenter.ViewInterface> {

    private var mView: V? = null
    private val mSubscription = CompositeDisposable()

    open fun onAttach(view: V) {
        mView = view
    }

    open fun viewResumed() {}

    fun addSubscription(d: Disposable) {
        mSubscription.add(d)
    }

    open fun viewPaused(){
        mSubscription.clear()
    }

    open fun onDetach() {
        mView = null
    }

    protected fun getView() : V {
        return mView!!
    }

    interface ViewInterface

}