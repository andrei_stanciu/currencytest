package com.`as`.currency.base

import android.app.Application
import com.`as`.currency.R
import com.`as`.currency.di.*
import com.`as`.currency.network.ApiConfig

class App : Application() {

    companion object {
        private var instance: App? = null
        fun getInstance(): App {
            return instance!!
        }
    }

    private lateinit var appComponent: AppComponent
    private lateinit var currencyComponent: CurrencyComponent

    override fun onCreate() {
        super.onCreate()
        instance = this

        val apiConfig = ApiConfig(getString(R.string.base_api_url))

        appComponent = DaggerAppComponent
            .builder()
            .setAppModule(AppModule(this))
            .setApiModule(ApiModule(apiConfig))
            .build()
    }

    fun setCurrencyComponent() {
        currencyComponent = appComponent.currencyComponent(CurrencyModule())
    }

    fun currencyComponent(): CurrencyComponent {
        return currencyComponent
    }
}