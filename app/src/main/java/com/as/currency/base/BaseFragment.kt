package com.`as`.currency.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import javax.inject.Inject

abstract class BaseFragment <V: BasePresenter.ViewInterface, P: BasePresenter<V>> : Fragment() {

    @Inject lateinit var mPresenter: P

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.onAttach(this as V)
    }

    override fun onResume() {
        super.onResume()
        mPresenter.viewResumed()
    }

    override fun onPause() {
        super.onPause()
        mPresenter.viewPaused()
    }

    override fun onDestroyView() {
        mPresenter.onDetach()
        super.onDestroyView()
    }
}