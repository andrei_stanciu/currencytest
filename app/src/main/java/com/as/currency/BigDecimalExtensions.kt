package com.`as`.currency

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

val df = DecimalFormat("#.###")

fun BigDecimal.divideBy(divisor: BigDecimal): BigDecimal = this.divide(divisor, 3, RoundingMode.HALF_UP)
fun BigDecimal.format(): String = df.format(this)