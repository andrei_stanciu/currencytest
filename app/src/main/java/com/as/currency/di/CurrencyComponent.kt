package com.`as`.currency.di

import com.`as`.currency.currencies.CurrencyFragment
import com.`as`.currency.di.scopes.ActivityScope
import dagger.Subcomponent

@Subcomponent(modules = [CurrencyModule::class])
@ActivityScope
interface CurrencyComponent {

    fun inject(currencyFragment: CurrencyFragment)

}