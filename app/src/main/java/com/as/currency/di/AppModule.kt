package com.`as`.currency.di

import android.content.Context
import com.`as`.currency.currencies.CurrencyNameProvider
import com.`as`.currency.currencies.FlagResourceProvider
import com.`as`.currency.di.scopes.AppScope
import dagger.Module
import dagger.Provides

@Module
class AppModule (private val appContext: Context) {

    @Provides
    @AppScope
    fun provideContext() : Context {
        return appContext
    }

    @Provides
    @AppScope
    fun provideFlagResProvider() : FlagResourceProvider {
        return FlagResourceProvider()
    }

    @Provides
    @AppScope
    fun provideCurrencyNameProvider() : CurrencyNameProvider {
        return CurrencyNameProvider()
    }
}