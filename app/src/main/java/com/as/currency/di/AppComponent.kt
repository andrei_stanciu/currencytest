package com.`as`.currency.di

import com.`as`.currency.di.scopes.AppScope
import dagger.Component

@Component(modules = [AppModule::class, ApiModule::class])
@AppScope
interface AppComponent {

    fun currencyComponent(currencyModule: CurrencyModule): CurrencyComponent

    @Component.Builder
    interface Builder {

        fun setAppModule(appModule: AppModule): Builder

        fun setApiModule(apiModule: ApiModule): Builder

        fun build(): AppComponent
    }
}