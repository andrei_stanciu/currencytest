package com.`as`.currency.di

import com.`as`.currency.currencies.CurrencyNameProvider
import com.`as`.currency.currencies.FlagResourceProvider
import com.`as`.currency.currencies.model.CurrenciesMapper
import com.`as`.currency.di.scopes.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class CurrencyModule {

    @Provides
    @ActivityScope
    fun provideCurrenciesMapper(flagResourceProvider: FlagResourceProvider,
                                currencyNameProvider: CurrencyNameProvider
    ): CurrenciesMapper {
        return CurrenciesMapper(flagResourceProvider, currencyNameProvider)
    }

}