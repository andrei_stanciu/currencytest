package com.`as`.currency.di

import com.`as`.currency.di.scopes.AppScope
import com.`as`.currency.network.ApiConfig
import com.`as`.currency.network.RetrofitCurrencyApi
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApiModule (private val apiConfig: ApiConfig) {

    @Provides
    @AppScope
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @AppScope
    fun provideOKHttpClient() : OkHttpClient {

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.apply { loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY };

        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @AppScope
    fun provideCurrencyApi(okHttpClient: OkHttpClient,
                           converterFactory: Converter.Factory) : RetrofitCurrencyApi {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(apiConfig.baseUrl)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
            .create(RetrofitCurrencyApi::class.java)
    }

}