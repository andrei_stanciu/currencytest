package com.`as`.currency.currencies.model

import java.math.BigDecimal

data class Currency (
    val code: String,
    val name: String,
    val flag: Int,

    var rate: BigDecimal
)