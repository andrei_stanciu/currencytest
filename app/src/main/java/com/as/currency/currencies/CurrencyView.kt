package com.`as`.currency.currencies

import com.`as`.currency.base.BasePresenter
import com.`as`.currency.currencies.model.CurrenciesResponse

interface CurrencyView : BasePresenter.ViewInterface {
    fun updateCurrencyList(currenciesResponse: CurrenciesResponse)
    fun showProgressBar()
    fun showError()
}