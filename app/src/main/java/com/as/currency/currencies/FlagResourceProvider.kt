package com.`as`.currency.currencies

import com.`as`.currency.R

class FlagResourceProvider {

    private val flagMap = mapOf (
        "EUR" to R.drawable.eur,
        "AUD" to R.drawable.aud,
        "BGN" to R.drawable.bgn,
        "BRL" to R.drawable.brl,
        "CAD" to R.drawable.cad,
        "CHF" to R.drawable.chf,
        "CNY" to R.drawable.cny,
        "CZK" to R.drawable.czk,
        "DKK" to R.drawable.dkk,
        "GBP" to R.drawable.gbp,
        "HKD" to R.drawable.hkd,
        "HRK" to R.drawable.hrk,
        "HUF" to R.drawable.huf,
        "IDR" to R.drawable.idr,
        "ILS" to R.drawable.ils,
        "INR" to R.drawable.inr,
        "ISK" to R.drawable.isk,
        "JPY" to R.drawable.jpy,
        "KRW" to R.drawable.krw,
        "MXN" to R.drawable.mxn,
        "MYR" to R.drawable.myr,
        "NOK" to R.drawable.nok,
        "NZD" to R.drawable.nzd,
        "PHP" to R.drawable.php,
        "PLN" to R.drawable.pln,
        "RON" to R.drawable.ron,
        "RUB" to R.drawable.rub,
        "SEK" to R.drawable.sek,
        "SGD" to R.drawable.sgn,
        "THB" to R.drawable.thb,
        "USD" to R.drawable.usd,
        "ZAR" to R.drawable.zar
    )

    fun getFlagRes(code: String): Int {
        return flagMap.getOrElse(code) { 0 }
    }
}