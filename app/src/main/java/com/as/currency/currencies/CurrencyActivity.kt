package com.`as`.currency.currencies

import android.os.Bundle
import com.`as`.currency.R
import com.`as`.currency.base.App
import com.`as`.currency.base.BaseActivity

class CurrencyActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.activity_currency_container, CurrencyFragment.newInstance())
                .commitAllowingStateLoss()
        }
    }

    override fun inject() {
        App.getInstance().setCurrencyComponent()
    }

}
