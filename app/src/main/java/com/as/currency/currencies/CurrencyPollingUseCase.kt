package com.`as`.currency.currencies

import com.`as`.currency.currencies.model.CurrenciesResponse
import com.`as`.currency.di.scopes.AppScope
import com.`as`.currency.network.RetrofitCurrencyApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AppScope
class CurrencyPollingUseCase @Inject constructor(private val currencyApi: RetrofitCurrencyApi) {

    companion object {
        const val POLL_TIME_IN_SECONDS = 1L
        const val RETRY_TIME_IN_SECONDS = 3L
    }

    private var currenciesResponse: CurrenciesResponse? = null

    fun pollCurrencies(onNext: Consumer<CurrenciesResponse>, onError: Consumer<Throwable>) : Disposable {

        //If the request succeeds, but at a later time the connection is lost
        //If the activity gets recreated, we will at least have something to show
        currenciesResponse?.let {
            onNext.accept(currenciesResponse)
        }

        return currencyApi.getCurrencies()
            .observeOn(AndroidSchedulers.mainThread())
            .repeatWhen {
                it.delay(POLL_TIME_IN_SECONDS, TimeUnit.SECONDS)
            }
            .doOnNext {
                currenciesResponse = it.copy(rates = it.rates.toMutableMap())
            }
            .doOnError {
                //Even though we are retrying, we still want the presenters to know if an error occurred
                onError.accept(it)
            }
            .retryWhen {
                it.delay(RETRY_TIME_IN_SECONDS, TimeUnit.SECONDS)
            }
            .subscribe(onNext)
    }
}