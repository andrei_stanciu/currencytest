package com.`as`.currency.currencies

import com.`as`.currency.base.BasePresenter
import io.reactivex.functions.Consumer
import javax.inject.Inject

class CurrencyPresenter @Inject constructor(private val pollingUseCase: CurrencyPollingUseCase) : BasePresenter<CurrencyView>() {

    override fun onAttach(view: CurrencyView) {
        super.onAttach(view)

        getView().showProgressBar()
    }

    override fun viewResumed() {
        super.viewResumed()

        addSubscription(pollingUseCase.pollCurrencies(
            Consumer {
                getView().updateCurrencyList(it)
            },
            Consumer {
                getView().showError()
            }))
    }
}