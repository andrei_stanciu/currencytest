package com.`as`.currency.currencies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.`as`.currency.R
import com.`as`.currency.base.App
import com.`as`.currency.base.BaseFragment
import com.`as`.currency.currencies.model.CurrenciesResponse
import kotlinx.android.synthetic.main.fragment_currency.view.*
import javax.inject.Inject

class CurrencyFragment : BaseFragment<CurrencyView, CurrencyPresenter>(), CurrencyView {

    companion object {
        fun newInstance(): CurrencyFragment {
            return CurrencyFragment()
        }
    }

    @Inject lateinit var currenciesAdapter: CurrencyAdapter
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mProgressBar: ProgressBar
    private lateinit var mErrorTV: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getInstance().currencyComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_currency, container, false)

        mRecyclerView = view.currency_recycler_view
        mProgressBar = view.currency_progress_bar
        mErrorTV = view.currency_error_tv

        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = currenciesAdapter

        return view
    }

    override fun updateCurrencyList(currenciesResponse: CurrenciesResponse) {
        mProgressBar.visibility = View.GONE
        mErrorTV.visibility = View.GONE
        currenciesAdapter.updateCurrencyList(currenciesResponse)
    }

    override fun showProgressBar() {
        mProgressBar.visibility = View.VISIBLE
    }

    override fun showError() {
        mErrorTV.visibility = View.VISIBLE
    }

}