package com.`as`.currency.currencies

import android.content.Context
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.`as`.currency.R
import com.`as`.currency.RateTextWatcher
import com.`as`.currency.currencies.model.CurrenciesMapper
import com.`as`.currency.currencies.model.CurrenciesResponse
import com.`as`.currency.currencies.model.Currency
import com.`as`.currency.divideBy
import com.`as`.currency.format
import kotlinx.android.synthetic.main.holder_currency.view.*
import java.math.BigDecimal
import javax.inject.Inject

class CurrencyAdapter  @Inject constructor (private val context: Context,
                                            private val mCurrenciesMapper: CurrenciesMapper)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val RATE = 42
    }

    private val layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    private var mItems = mutableListOf<Currency>()
    private var factor = BigDecimal.ONE

    private val factorChangedCallback: (String) -> Unit = { value ->
        val newFactor = if (value.isEmpty()) {
            BigDecimal.ZERO
        } else {
            BigDecimal(value).divideBy(mItems[0].rate)
        }

        //only update the UI if the text change affected the factor.
        //updating for changes like "1" -> "1." or "1." -> "1.0" would be a waste of resources
        if (factor.compareTo(newFactor) != 0) {
            factor = newFactor

            //update all ViewHolders except the first one, which is being edited by the user
            notifyItemRangeChanged(1, mItems.size - 1, RATE)
        }
    }

    fun updateCurrencyList(currenciesResponse: CurrenciesResponse) {
        val originalSize = mItems.size
        val originalRate = if (mItems.size > 0) mItems[0].rate else BigDecimal.ONE
        mItems = mCurrenciesMapper.getItemsFromResponse(mItems, currenciesResponse)

        if (originalSize == 0) {
            notifyItemRangeInserted(0, mItems.size)
        } else {
            factor = factor.multiply(originalRate).divideBy(mItems[0].rate)

            if (originalSize == mItems.size) {
                notifyItemRangeChanged(1, mItems.size - 1, RATE)
            } else {
                //if the list size changed, currency pairs have been added/removed
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CurrencyViewHolder(layoutInflater.inflate(R.layout.holder_currency, parent, false), factorChangedCallback)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CurrencyViewHolder).currencyCodeTV.text = mItems[position].code
        holder.currencyNameTV.text = mItems[position].name
        holder.flagIV.setImageResource(mItems[position].flag)

        if (factor == BigDecimal.ZERO) {
            holder.currencyAmountET.setText("")
        } else {
            holder.currencyAmountET.setText((mItems[position].rate.multiply(factor)).format())
        }

        holder.itemView.setOnClickListener {
            //if we give the EditText focus onClick, then when moving the ViewHolder to the top, the RecyclerView
            //will automatically scroll to keep it visible.
            holder.currencyAmountET.requestFocus()
            holder.currencyAmountET.setSelection(holder.currencyAmountET.text.length)
            inputMethodManager.showSoftInput(holder.currencyAmountET, InputMethodManager.SHOW_IMPLICIT)
        }

        holder.currencyAmountET.setOnFocusChangeListener { editText, hasFocus ->
            if (hasFocus) {
                //when the EditText gains focus, move the ViewHolder to the top of the list
                val pos = holder.adapterPosition
                val currency = mItems[pos]
                mItems.removeAt(pos)
                mItems.add(0, currency)
                notifyItemMoved(pos, 0)
            } else {
                val rateStr = (editText as EditText).text.toString()
                if (rateStr.isNotEmpty()) {
                    val rate = BigDecimal(editText.text.toString())
                    val formattedRate = rate.format()

                    //this will trigger the text watcher, so only do it if strictly necessary
                    if (!formattedRate.equals(rateStr, true)) {
                        editText.setText(rate.format())
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNotEmpty() && (payloads[0] as Int) == RATE) {
            if (factor == BigDecimal.ZERO) {
                (holder as CurrencyViewHolder).currencyAmountET.setText("")
            } else {
                (holder as CurrencyViewHolder).currencyAmountET.setText((mItems[position].rate.multiply(factor)).format())
            }
            return
        }

        super.onBindViewHolder(holder, position, payloads)
    }

    internal class CurrencyViewHolder(view: View, factorChangedCallback: (String)->Unit) : RecyclerView.ViewHolder(view) {
        internal val flagIV: ImageView = view.holder_currency_flag_img
        internal val currencyCodeTV: TextView = view.holder_currency_code_tv
        internal val currencyNameTV: TextView = view.holder_currency_name_tv
        internal val currencyAmountET: EditText = view.holder_currency_amount_et

        init {
            currencyAmountET.addTextChangedListener(RateTextWatcher(currencyAmountET, factorChangedCallback))

            //Copy/pasting text into the input fields can be a massive source of unexpected behaviour.
            //Safest bet is to just disable it.
            currencyAmountET.customSelectionActionModeCallback = object : ActionMode.Callback {
                override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {return false}
                override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {return false}
                override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {return false}
                override fun onDestroyActionMode(mode: ActionMode?) {}
            }
        }
    }
}