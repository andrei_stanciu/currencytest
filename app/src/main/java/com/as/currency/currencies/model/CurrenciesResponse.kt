package com.`as`.currency.currencies.model

data class CurrenciesResponse(
    val baseCurrency: String,
    val rates: MutableMap<String, String>
)