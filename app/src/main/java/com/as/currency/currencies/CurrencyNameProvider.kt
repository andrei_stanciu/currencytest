package com.`as`.currency.currencies

class CurrencyNameProvider {

    private val nameMap = mapOf (
        "EUR" to "Euro",
        "AUD" to "Australian Dollar",
        "BGN" to "Bulgarian Lev",
        "BRL" to "Brazilian Real",
        "CAD" to "Canadian Dollar",
        "CHF" to "Swiss Franc",
        "CNY" to "Chinese Yuan",
        "CZK" to "Czech Koruna",
        "DKK" to "Danish Krone",
        "GBP" to "Pound sterling",
        "HKD" to "Hong Kong Dollar",
        "HRK" to "Croatian Kuna",
        "HUF" to "Hungarian Forint",
        "IDR" to "Indonesian Rupiah",
        "ILS" to "Israeli New Shekel",
        "INR" to "Indian Rupee",
        "ISK" to "Icelandic Króna",
        "JPY" to "Japanese Yen",
        "KRW" to "South Korean won",
        "MXN" to "Mexican Peso",
        "MYR" to "Malaysian Ringgit",
        "NOK" to "Norwegian Krone",
        "NZD" to "New Zealand Dollar",
        "PHP" to "Philippine peso",
        "PLN" to "Poland złoty",
        "RON" to "Romanian Leu",
        "RUB" to "Russian Ruble",
        "SEK" to "Swedish Krona",
        "SGD" to "Singapore Dollar",
        "THB" to "Thai Baht",
        "USD" to "United States Dollar",
        "ZAR" to "South African Rand")

    fun getCurrencyName(code: String): String {
        return nameMap.getOrElse(code) { code }
    }
}