package com.`as`.currency.currencies.model

import com.`as`.currency.currencies.CurrencyNameProvider
import com.`as`.currency.currencies.FlagResourceProvider
import java.math.BigDecimal

class CurrenciesMapper (private val flagResourceProvider: FlagResourceProvider,
                        private val currencyNameProvider: CurrencyNameProvider) {

    fun getItemsFromResponse(items: MutableList<Currency>, currenciesResponse: CurrenciesResponse): MutableList<Currency> {
        val ratesMap = currenciesResponse.rates
        if (items.size == 0) {
            //if this is the first call, make sure to include the base currency
            items.add(Currency(currenciesResponse.baseCurrency,
                currencyNameProvider.getCurrencyName(currenciesResponse.baseCurrency),
                flagResourceProvider.getFlagRes(currenciesResponse.baseCurrency),
                BigDecimal("1.0")))
        }

        //update the rate for all currencies we currently have, while removing them from the map
        //OR from the list, if they are no longer available in the backend, for whatever reason.
        items.forEach { currency ->
            if (ratesMap[currency.code] != null) {
                currency.rate = BigDecimal(ratesMap[currency.code])
                ratesMap.remove(currency.code)
            } else if (currency.code != currenciesResponse.baseCurrency) {
                //The base currency is not in the map, so make sure not to remove it
                items.remove(currency)
            }
        }

        //if there is anything currency left in the map, it means it wasn't available before, so we add it
        ratesMap.keys.forEach { currencyCode ->
            //TODO set correct name & flag
            items.add(Currency(currencyCode,
                currencyNameProvider.getCurrencyName(currencyCode),
                flagResourceProvider.getFlagRes(currencyCode),
                BigDecimal(ratesMap[currencyCode])))
        }

        return items
    }
}