package com.`as`.currency

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class RateTextWatcher(private val editText: EditText, val factorChangedCallback: (String)->Unit) : TextWatcher {

    private lateinit var textBefore: String
    private var selectionBefore: Int = 0

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        textBefore = s.toString()
        selectionBefore = editText.selectionStart
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable) {
        var trimmed = false
        if (s.contains(Constants.SEPARATOR)) {
            val fractionPartLength = s.substring(s.indexOf(Constants.SEPARATOR) + 1).length
            if (fractionPartLength > 3) {
                editText.setText(textBefore)
                editText.setSelection(selectionBefore - 1)
                trimmed = true
            }
        }

        if (!trimmed && editText.hasFocus()) {
            factorChangedCallback(editText.text.toString())
        }
    }
}