package com.`as`.currency

import com.`as`.currency.currencies.CurrencyPollingUseCase
import com.`as`.currency.currencies.CurrencyPresenter
import com.`as`.currency.currencies.CurrencyView
import com.`as`.currency.currencies.model.CurrenciesResponse
import com.nhaarman.mockitokotlin2.*
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import org.junit.Test

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CurrencyPresenterTest {

    @Mock private lateinit var mPollingUseCase: CurrencyPollingUseCase
    @Mock private lateinit var mDisposable: Disposable
    @Mock private lateinit var mThrowable: Throwable
    @Mock private lateinit var mView: CurrencyView

    private lateinit var mCurrencyPresenter: CurrencyPresenter

    @Before
    fun setup() {
        mCurrencyPresenter = CurrencyPresenter(mPollingUseCase)
        mCurrencyPresenter.onAttach(mView)
    }

    @Test
    fun onAttach_showsProgressBar() {
        verify(mView).showProgressBar()
    }

    @Test
    fun viewResumed_success_displays_currencies() {
        val currenciesResponse = CurrenciesResponse("", mutableMapOf())

        `when`(mPollingUseCase.pollCurrencies(any(), any()))
            .doAnswer {
                (it.arguments[0] as Consumer<CurrenciesResponse>).accept(currenciesResponse)
                return@doAnswer mDisposable
            }

        mCurrencyPresenter.viewResumed()

        verify(mView).showProgressBar()
        verify(mView).updateCurrencyList(currenciesResponse)
    }

    @Test
    fun viewResumed_exception_displays_error() {
        `when`(mPollingUseCase.pollCurrencies(any(), any()))
            .doAnswer {
                (it.arguments[1] as Consumer<Throwable>).accept(mThrowable)
                return@doAnswer mDisposable
            }

        mCurrencyPresenter.viewResumed()

        verify(mView).showProgressBar()
        verify(mView).showError()
    }

    @Test
    fun viewResumed_shows_results_and_exceptions_over_multiple_ticks() {
        val currenciesResponse = CurrenciesResponse("", mutableMapOf())

        `when`(mPollingUseCase.pollCurrencies(any(), any()))
            .doAnswer {
                (it.arguments[0] as Consumer<CurrenciesResponse>).accept(currenciesResponse)
                (it.arguments[1] as Consumer<Throwable>).accept(mThrowable)
                (it.arguments[0] as Consumer<CurrenciesResponse>).accept(currenciesResponse)
                return@doAnswer mDisposable
            }

        mCurrencyPresenter.viewResumed()

       mView.inOrder {
           verify().showProgressBar()
           verify().updateCurrencyList(currenciesResponse)
           verify().showError()
           verify().updateCurrencyList(currenciesResponse)
           verifyNoMoreInteractions()
       }
    }
}
